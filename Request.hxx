#pragma once

#include <QString>
#include <QNetworkRequest>

namespace Network
{

class Request
{
public:
    Request(QString = QString());

    QString address() const;
    void setAddress(QString);

    void addParam(QString, QVariant);
    bool removeParam(QString);

    QStringList paramsNames() const;
    QMap<QString, QString> params() const;

    QUrl url(bool = true) const;
    QNetworkRequest request(bool = true) const;
    QByteArray data() const;
signals:

public slots:

private:
    QString m_address;
    QMap<QString, QString> m_params;
};

}
