#include "RequestSender.hxx"

#include <QTimer>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QThread>

namespace Network
{

RequestSender::RequestSender(qint64 maxWaitTime)
{
    setMaxWaitTime(maxWaitTime);
    m_error = RequestError::NoError;
}

RequestSender::~RequestSender()
{

}

void RequestSender::setProxy(const QNetworkProxy& proxy)
{
    m_proxy = proxy;
}

QByteArray RequestSender::get(Request &request)
{
    return sendRequest(request, true);
}


QByteArray RequestSender::post(Request &request)
{
    return sendRequest(request, false);
}

QByteArray RequestSender::getWhileSuccess(Request& request, int maxCount)
{
    return sendWhileSuccess(request, maxCount, true);
}

QByteArray RequestSender::postWhileSuccess(Request& request, int maxCount)
{
    return sendWhileSuccess(request, maxCount, false);
}

void RequestSender::setMaxWaitTime(qint64 max)
{
    m_maxWaitTime = max;
}

qint64 RequestSender::maxWaitTime() const
{
    return m_maxWaitTime;
}

RequestSender::RequestError RequestSender::error() const
{
    return m_error;
}

QByteArray RequestSender::sendRequest(Request& request, bool getRequest /*= true*/)
{
    QTimer timer;
    timer.setInterval(m_maxWaitTime);
    timer.setSingleShot(true);

    QEventLoop loop;
    QSharedPointer<QNetworkAccessManager> manager(new QNetworkAccessManager);
    manager->setProxy(m_proxy);

    QNetworkReply* reply = getRequest ? manager->get(request.request()) :
                                        manager->post(request.request(false), request.data());

#if defined(NETWORK_SHOW_SEND_REQUESTS)
    if (getRequest)
        qDebug() << "[GET] " <<  request.request().url().toString();
    else
        qDebug() << "[POST]" << request.request(false).url().toString() << request.data();
#endif

    QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    QObject::connect(&timer, &QTimer::timeout, reply, &QNetworkReply::abort);

    timer.start();
    loop.exec();

    QByteArray data;

    if (reply->isFinished() && reply->error() == QNetworkReply::NoError)
    {
        data = reply->readAll();
        m_error = RequestError::NoError;
    }
    else
    {
        m_error = RequestError::TimeoutError;
    }

    reply->deleteLater();

#if defined(NETWORK_SHOW_SEND_REQUESTS)
    qDebug() << "[ANSWER]" << data;
#endif

    return data;
}

QByteArray RequestSender::sendWhileSuccess(Request& request, int maxCount, bool getRequest)
{
    if (maxCount < 0)
        throw QString(__LINE__ + " " __FILE__);

    int c = 0;
    QByteArray answer;

    while (c < maxCount)
    {
        c++;
        answer = getRequest ? get(request) : post(request);

        if (error() == RequestError::NoError)
            break;

        qDebug() << "Ошибка при отправке запроса. Код ошибки - " << static_cast<int>(error()) << ". Повторная отправка запроса через 2 секунды" << endl;
        QThread::currentThread()->msleep(2000);
    }

    return answer;
}

}
