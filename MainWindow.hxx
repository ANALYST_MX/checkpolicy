#pragma once

#include <QMainWindow>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_pushButtonPolNum_clicked();

  void on_pushButtonTempCertif_clicked();

private:
  Ui::MainWindow *m_ui;
};
