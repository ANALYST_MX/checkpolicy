#pragma once

#include <QObject>

#include "Request.hxx"

namespace Network
{

class NetworkManager
        : public QObject
{
    Q_OBJECT
public:
    NetworkManager() = default;
    QByteArray get(QString);
    QByteArray post(QString);
    QByteArray get(Request);
    QByteArray post(Request);
    bool isOnline();

signals:

public slots:
};

}
