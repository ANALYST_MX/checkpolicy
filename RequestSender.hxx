#pragma once

#include <QObject>
#include <QNetworkProxy>

#include "Request.hxx"

namespace Network
{

class RequestSender
        : public QObject
{
    Q_OBJECT
public:
    enum class RequestError : uint8_t
    {
        NoError = 0,
        TimeoutError
    };

    RequestSender(qint64 = 35000);
    ~RequestSender();

    void setProxy(const QNetworkProxy &);

    QByteArray get(Request &);
    QByteArray post(Request &);
    QByteArray getWhileSuccess(Request &, int = 2);
    QByteArray postWhileSuccess(Request &, int = 2);

    void setMaxWaitTime(qint64);

    qint64 maxWaitTime() const;
    RequestError error() const;

signals:

public slots:
private:
    QByteArray sendRequest(Request& request, bool getRequest = true);
    QByteArray sendWhileSuccess(Request& request, int maxCount = 2, bool getRequest = true);

private:
    qint64 m_maxWaitTime;
    RequestError m_error;
    QNetworkProxy m_proxy;
};

}
