#include "NetworkManager.hxx"

#include <QNetworkInterface>

#include "RequestSender.hxx"

namespace Network
{

QByteArray NetworkManager::get(QString url)
{
    Request request(url);
    return RequestSender(5000).get(request);
}

QByteArray NetworkManager::post(QString url)
{
    Request request(url);
    return RequestSender(5000).post(request);
}

QByteArray NetworkManager::get(Request reqUrl)
{
    return RequestSender(5000).get(reqUrl);
}

QByteArray NetworkManager::post(Request reqUrl)
{
    return RequestSender(5000).post(reqUrl);
}

bool NetworkManager::isOnline()
{
    QList<QNetworkInterface> iFaces = QNetworkInterface::allInterfaces();
    bool result = false;
    for (int i = 0; i < iFaces.count(); i++)
    {
        QNetworkInterface iface = iFaces.at(i);
        if ( iface.flags().testFlag(QNetworkInterface::IsUp)
             && !iface.flags().testFlag(QNetworkInterface::IsLoopBack))
        {
            qInfo() << "name:" << iface.name() << endl
                    << "ip addresses:" << endl
                    << "mac:" << iface.hardwareAddress() << endl;
            for (int j=0; j<iface.addressEntries().count(); j++)
            {
                qInfo() << iface.addressEntries().at(j).ip().toString()
                        << " / " << iface.addressEntries().at(j).netmask().toString() << endl;
                if (result == false)
                    result = true;
            }
        }
    }
    return result;
}

}
