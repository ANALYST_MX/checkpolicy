#-------------------------------------------------
#
# Project created by QtCreator 2016-01-27T11:20:00
#
#-------------------------------------------------

QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CheckPolicy
TEMPLATE = app


SOURCES += main.cxx\
        MainWindow.cxx \
    Request.cxx \
    RequestSender.cxx \
    NetworkManager.cxx

HEADERS  += MainWindow.hxx \
    Request.hxx \
    RequestSender.hxx \
    NetworkManager.hxx

FORMS    += MainWindow.ui

CONFIG += c++11
