#include "MainWindow.hxx"

#include "ui_MainWindow.h"

#include "NetworkManager.hxx"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete m_ui;
}

void MainWindow::on_pushButtonPolNum_clicked()
{
    Network::Request reqUrl;
    reqUrl.setAddress(QString("http://62.133.163.218:4305/ServiceModelSamples/service/setdate?ExchangeType=PolNum&PolNumber=%1").arg(m_ui->lineEditPolNum->text()));
    try
    {
        QByteArray resData = Network::NetworkManager().get(reqUrl);
        if(resData.size())
            m_ui->textEditPolNum->setText(QString::fromUtf8(resData));
    }
    catch(...)
    {
    }
}

void MainWindow::on_pushButtonTempCertif_clicked()
{
    Network::Request reqUrl;
    reqUrl.setAddress(QString("http://62.133.163.218:4305/ServiceModelSamples/service/setdate?ExchangeType=TempCertif&PolNumber=%1").arg(m_ui->lineEditTempCertif->text()));
    try
    {
        QByteArray resData = Network::NetworkManager().get(reqUrl);
        if(resData.size())
            m_ui->textEditTempCertif->setText(QString::fromUtf8(resData));
    }
    catch(...)
    {
    }
}
