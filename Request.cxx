#include "Request.hxx"

#include <QUrlQuery>

namespace Network
{

Request::Request(QString address)
{
    setAddress(address);
}

QString Request::address() const
{
    return m_address;
}

void Request::setAddress(QString address)
{
    for (QPair<QString, QString> value : QUrlQuery(QUrl(address)).queryItems())
        addParam(value.first, value.second);
    m_address = address;
}

void Request::addParam(QString name, QVariant value)
{
    m_params[name] = value.toString();
}

bool Request::removeParam(QString name)
{
    if (false == m_params.contains(name))
        return false;
    m_params.remove(name);
    return true;
}

QStringList Request::paramsNames() const
{
    return m_params.keys();
}

QMap<QString, QString> Request::params() const
{
    return m_params;
}

QUrl Request::url(bool forGetRequest) const
{
    QUrl url(address());
    if (forGetRequest)
        url.setQuery(data());
    return url;
}

QNetworkRequest Request::request(bool forGetRequest) const
{
    QNetworkRequest r(url(forGetRequest));

    if (!forGetRequest)
    {
        r.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    }

    return r;
}

QByteArray Request::data() const
{
    auto b = m_params.begin();
    auto e = m_params.end();

    QByteArray byteArrayData;

    while (b != e)
    {
        byteArrayData.append(b.key());
        byteArrayData.append('=');
        byteArrayData.append(b.value());
        byteArrayData.append('&');

        b++;
    }

    byteArrayData.chop(1);

    return byteArrayData;
}

}
